import random


class ViewMatch:
    def __init__(self, matchs):
        self.matchs = matchs

    def listed_match(self):
        """Displays the list of matches in the round and prompts the user to
        enter the id of the match whose score is to be updated"""

        # Retrieves the last one from the dictionary
        last_key, last_content = list(self.matchs.items())[-1]
        last_round = last_key

        # Retrieves the contents of the last dictionary key
        last_dict = self.matchs[last_round]

        print("")
        print(f"**Liste des matchs du {last_round}**")
        list_of_key = []
        jump = 0
        for key, value in last_dict.items():
            if jump < 2:
                jump += 1
                continue
            list_of_key.append(key)
            player1 = value[0][0]
            player2 = value[1][0]
            score1 = value[0][1]
            score2 = value[1][1]
            print(f"{key} : {player1} - {score1} vs {player2} - {score2}")
        return list_of_key

    def white_player(self):
        """Displays which players play white for set matches"""

        last_key, last_content = list(self.matchs.items())[-1]
        last_round = last_key

        print("")
        print(f"** Selection des couleurs des pièces pour les matchs du round {last_round}**")

        # Retrieves the contents of the last dictionary key
        last_dict = self.matchs[last_round]
        list_of_key = []
        jump = 0
        for key, value in last_dict.items():
            if jump < 2:
                jump += 1
                continue
            list_of_key.append(key)
            player1 = value[0][0]
            player2 = value[1][0]
            list_random = []
            list_random.append(player1)
            list_random.append(player2)
            random_player = random.choice(list_random)
            print(f"{key} : {player1} vs {player2} = {random_player} joue les blancs !")

    def input_result(self):
        """Asks user for match ID"""
        print("Selectionnez l'id du match pour entrer le résultat")
        print("Exemple : entrer '1' si vous voulez entrer le résultat du match 1")
        id_match = input(" : ")
        return id_match

    def match_already_update(self):
        """Display message indicate match already update"""
        print("Le résultat de ce match a déjà été mis à jour")

    def display_match(self, match_result, result):
        """Display result of match"""
        player1 = match_result[0][0]
        player2 = match_result[1][0]
        if result == 1:
            print(f"Résultat du match : {player1} à gagné contre {player2}")
        elif result == 2:
            print(f"Résultat du match : {player2} à gagné contre {player1}")
        elif result == 3:
            print(f"Résultat du match : {player1} et {player2} ont fait match nul !")

    def result_asked(self, player1, player2):
        """Asks the user which player has won or if there is a draw"""
        print("Qui a gagné ?")
        print(f"Tape '1' si {player1} à gagné")
        print(f"Tape '2' si {player2} à gagné")
        print("Tape '3' si il y a match nul")
        result = int(input(" : "))
        return result
