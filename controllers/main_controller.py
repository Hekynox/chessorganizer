from models.data import FolderCreate


def folder_needed():
    """Create program files if necessary"""
    data = FolderCreate()

    data.folder_data()
    data.folder_report()
    data.folder_tournament()
    data.folder_checking()
