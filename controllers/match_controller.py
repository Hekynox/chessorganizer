from models.matchmaking import DataMatch, UpdateMatch
from views.match_view import ViewMatch
from datetime import datetime
import random


class MatchController:
    def __init__(self, player1, player2, score1=0, score2=0):
        self.player1 = player1
        self.player2 = player2
        self.score_player1 = score1
        self.score_player2 = score2

    def matched(self):
        """Create a tuple containing two lists, each containing two elements: player / score"""
        player1 = []
        player2 = []
        player1.append(self.player1)
        player1.append(self.score_player1)
        player2.append(self.player2)
        player2.append(self.score_player2)
        match = (player1, player2)
        return match

    def result(self, view_obj):
        """Define the result of a match"""
        result = int
        while result != 1 and result != 2 and result != 3:
            result = int(view_obj.result_asked(self.player1, self.player2))
        if result == 1:
            self.win(result)
        elif result == 2:
            self.win(result)
        elif result == 3:
            self.draw()
        match_result = self.return_match()
        view_obj.display_match(match_result, result)
        return match_result

    def return_match(self):
        """Return the match with the updated score"""
        match_result_p1 = []
        match_result_p2 = []
        match_result_p1.append(self.player1)
        match_result_p1.append(self.score_player1)
        match_result_p2.append(self.player2)
        match_result_p2.append(self.score_player2)
        match_result = (match_result_p1, match_result_p2)
        return match_result

    def reverse_matched(self):
        """Invert the establishment of the tuple: Player 1 as the first and Player 2 as the second"""
        player1 = []
        player2 = []
        player1.append(self.player2)
        player1.append(self.score_player2)
        player2.append(self.player1)
        player2.append(self.score_player1)
        match = (player1, player2)
        return match

    def win(self, result):
        """Add 1 point for the winning player"""
        if result == 1:
            self.score_player1 += 1
        elif result == 2:
            self.score_player2 += 1

    def draw(self):
        """Add 0.5 points to each player if the match is a draw"""
        self.score_player1 += 0.5
        self.score_player2 += 0.5


class Matchmaking(MatchController):
    def __init__(self, list_players_match, id_tournament="", currently_round=0):
        self.list_players_match = list_players_match
        self.id_tournament = id_tournament
        self.currently_round = currently_round
        self.startendtime = datetime.now().strftime("%Y-%m-%d_%Hh%M")

    def match_first_round(self):
        """Etabli les matchs du premier round"""
        list_player_match = []

        # Create each match for the round and add them to a list
        for i in self.list_players_match:
            make_match = MatchController(i[0], i[1])
            making_match = make_match.matched()
            list_player_match.append(making_match)
        data_match = DataMatch(self.id_tournament, self.currently_round, list_player_match)
        data_match.save_match(self.startendtime)

    def select_match(self, id):
        """Display the list of matches for the user to choose from until
        all match results are entered

        - A match for which the result has already been entered cannot be modified again.
        - Update the JSON file containing the matches each time a match result is entered"""
        verif = False
        list_input_id = []
        while verif is False:
            # Initialize a class to update the matches
            loading_match = UpdateMatch(id)

            # Load the file containing the matches
            load_match = loading_match.load_match_tournament()

            # Display the list of matches to the user
            view_match = ViewMatch(load_match)
            select = False
            while select is False:
                # Retrieve the list of keys for the matches
                list_of_key = view_match.listed_match()

                # Display the instructions to follow and retrieve user input
                id_match = view_match.input_result()
                name_match = f"match {id_match}"
                key_match = f"{name_match}"

                # Load file JSON "checking"
                data_checking = self.checking_loaded(None)

                # Check that the user input corresponds to an existing match
                if key_match in list_of_key:
                    select = True

                    # Verify that the user has not already entered the result of the match
                    if (key_match in list_input_id) or (key_match in data_checking):
                        view_match.match_already_update()
                    else:
                        list_input_id.append(key_match)

                        # Retrieve the key (the round)
                        round = list(load_match.keys())[-1]

                        # Retrieve the match chosen by the user
                        match_choice = load_match[round][key_match]

                        # Retrieve the names and scores of the players in the match
                        player1 = match_choice[0][0]
                        player2 = match_choice[1][0]
                        score1 = match_choice[0][1]
                        score2 = match_choice[1][1]
                        match_result = MatchController(player1, player2, score1, score2)

                        # Return the result of the match after modification
                        result = match_result.result(view_match)
                        load_match[round][key_match] = result
                        loading_match.update_match(load_match)

                        # Add the updated match to a JSON file
                        self.add_checking(None, key_match)

            # Compare user inputs with the list of match keys
            for i in list_of_key:
                for x in list_input_id:
                    # If all keys have been entered, return True
                    if x in i:
                        verif = True
                    # Otherwise, return False
                    else:
                        verif = False

    def sorted_list_score(self, list_player_score):
        """Return a list sorted by score in descending order"""

        # Sort the list in descending order based on the score
        sorted_list_score = sorted(list_player_score, key=lambda x: x[1], reverse=True)
        return sorted_list_score

    def recover_prev_matchs(self, data_matchs):
        """Retrieve the matches played in the previous round and store them in a list"""
        previous_matchs = []

        # Retrieve the data from the last round
        lats_key, last_content = list(data_matchs.items())[-1]
        data_round = last_content

        jump = 0

        # Iterate over the keys contained in "data_round" to retrieve the values
        for key, value in data_round.items():
            if jump < 2:
                jump += 1
                continue
            # Add the retrieved values to a list
            previous_matchs.append(value)
        return previous_matchs

    def recover_all_matchs(self, list_of_match):
        """Generate a list containing all the matches played in each round"""
        all_matchs = []
        for key, value in list_of_match.items():
            for key2, value2 in value.items():
                if key2 == "Date/Heure debut ":
                    continue
                elif key2 == "Date/Heure fin ":
                    continue
                else:
                    first_player = value2[0]
                    sncd_player = value2[1]
                    player1 = first_player[0]
                    player2 = sncd_player[0]
                    match = (player1, player2)
                    all_matchs.append(match)
        return all_matchs

    def list_player_score(self, previous_match_score):
        """Generate a list containing a sublist for each player and their scores"""
        list_player_score = []

        # Iterate over the list containing the tuples of each match
        for i in previous_match_score:
            # Iterate over the elements contained within each tuple
            for player_score in i:
                list_player_score.append(player_score)
        return list_player_score

    def recover_player(self, list_of_match):
        """Retrieve the players from the matches and store them in a list"""

        # Initialize the list that will contain the players
        list_of_player = []
        if list_of_match:
            # Index the list for player and score within the match tuple
            imatch = 0
            # index player
            iplayer = 0
            for j in list_of_match:
                p1_score = j[imatch]
                p2_score = j[imatch+1]
                list_of_player.append(p1_score[iplayer])
                list_of_player.append(p2_score[iplayer])
            return list_of_player
        else:
            return list_of_player

    def associate_player(self, sorted_list_score, all_matchs, previous_matchs):
        """Associate the players for the next matches according to the following conditions:

        - Pair up the players in order
        - Avoid duplicates from matches already played"""

        # Initialize the list that will contain the lists of already established matches
        match_establish = []
        listed_player = []

        # Index of the tuple containing [player, score]
        list_element = 0

        # Retrieve the number of matches to be established
        number_of_match = len(previous_matchs)

        # Loop until the number of matches to be established has been established
        while len(match_establish) != number_of_match:
            # Retrieve the list of players who already have a match established for the round to be organized
            list_of_player = self.recover_player(match_establish)

            # Retrieve the name of player 1 as "jp1" and player 2 as "jp2"
            jp1 = sorted_list_score[list_element][0]
            jp2 = sorted_list_score[list_element+1][0]

            verif = False
            verif1 = False
            verif2 = False

            # Loop to ensure that the match to be established has not already been established :
            # - For the next round
            # - In the previous rounds
            try:
                while verif is False:
                    incr_jp1 = 0
                    incr_jp2 = 0

                    # Loop to determine "jp1"
                    while verif1 is False:
                        if not list_of_player:
                            jp1 = sorted_list_score[list_element+incr_jp1][0]
                            p1 = sorted_list_score[list_element+incr_jp1]
                            verif1 = True
                        else:
                            if jp1 in list_of_player:
                                incr_jp1 += 1
                                jp1 = sorted_list_score[list_element+incr_jp1][0]
                            else:
                                # Keep the name of the player to compare with "jp2" later
                                jp1 = sorted_list_score[list_element+incr_jp1][0]

                                # Retrieve the list containing the player's name and their score for "p1"
                                p1 = sorted_list_score[list_element+incr_jp1]
                                verif1 = True

                    # Loop to determine "jp2"
                    while verif2 is False:
                        if not list_of_player:
                            jp2 = sorted_list_score[list_element+incr_jp2][0]
                            p2 = sorted_list_score[list_element+incr_jp2]

                            # Check that "jp2" does not match "jp1"
                            if jp2 == jp1:
                                incr_jp2 += 1
                                jp2 = sorted_list_score[list_element+incr_jp2][0]
                            else:
                                p2 = sorted_list_score[list_element+incr_jp2]

                                if jp2 not in [player[0] for player in match_establish]:
                                    verif2 = self.verify_match(p1[0], p2[0], all_matchs)
                                    verif = verif2
                                incr_jp2 += 1
                        else:
                            # Check that "jp2" does not match "jp1"
                            if jp2 == jp1:
                                incr_jp2 += 1
                                jp2 = sorted_list_score[list_element+incr_jp2][0]
                            else:
                                if jp2 in list_of_player:
                                    incr_jp2 += 1
                                    jp2 = sorted_list_score[list_element+incr_jp2][0]
                                else:
                                    # Retrieve the list containing the player's name and their score for "p2"
                                    p2 = sorted_list_score[list_element+incr_jp2]

                                    if jp2 not in [player[0] for player in match_establish]:
                                        verif2 = self.verify_match(p1[0], p2[0], all_matchs)
                                        verif = verif2
                                    incr_jp2 += 1

            # if program use all possibility
            except IndexError:
                for i in match_establish:
                    for p in i:
                        listed_player.append(p)
                verification = False
                while verification is False:
                    p1 = random.choice(sorted_list_score)
                    p2 = random.choice(sorted_list_score)
                    if (p1[0] == p2[0]) or (p2[0] == p1[0]):
                        continue
                    else:
                        match_1 = (p1, p2)
                        match_2 = (p2, p1)
                        if match_1 in match_establish or match_2 in match_establish:
                            continue
                        elif p1 in listed_player or p2 in listed_player:
                            continue
                        else:
                            verification = True

            # Initialize the instance of the match
            match = MatchController(p1[0], p2[0], p1[1], p2[1])

            # Create the match and add it to the list of matches for the future round
            matched = match.matched()
            match_establish.append(matched)
        return match_establish

    def verify_match(self, p1, p2, all_matchs):
        """Establish the match in both directions and check if it already exists
        in all matches that have been established in the tournament"""
        match_a = (p1, p2)
        match_b = (p2, p1)
        if (match_a not in all_matchs) and (match_b not in all_matchs):
            return True
        else:
            return False

    def generate_peer(self):
        """Generate the pairs of matches excluding Round 1"""
        data_match = DataMatch(self.id_tournament, self.currently_round, self.list_players_match)

        # Check if the matches of the round have already been established
        return_verify = data_match.verify_round()

        if return_verify is False:
            # Load the file containing the tournament matches
            match_loaded = data_match.load_match_tournament()

            # Retrieve a list of matches played in the previous round
            previous_matchs = self.recover_prev_matchs(match_loaded)

            # Retrieve a list of matches played in all rounds
            all_matchs = self.recover_all_matchs(match_loaded)

            # Retrieve a list containing sublists, each containing a player and their score
            list_player_score = self.list_player_score(previous_matchs)

            # Retrieve a list of players sorted by their scores (in descending order)
            sorted_list_score = self.sorted_list_score(list_player_score)

            # Return the list of matches established for the next round
            match_establish = self.associate_player(sorted_list_score, all_matchs, previous_matchs)

            # Save the established matches for the next round
            data_matchup = DataMatch(self.id_tournament, self.currently_round, match_establish)
            data_matchup.save_match(self.startendtime)
        else:
            pass

    def winner(self):
        """Return the winner of the tournament"""
        data_match = DataMatch(self.id_tournament, self.currently_round, self.list_players_match)

        match_loaded = data_match.load_match_tournament()

        previous_matchs = self.recover_prev_matchs(match_loaded)

        list_player_score = self.list_player_score(previous_matchs)

        sorted_list_score = self.sorted_list_score(list_player_score)

        winner_list = []

        winner = sorted_list_score[0][0]

        for i in sorted_list_score[1:]:
            if i[1] == sorted_list_score[0][1]:
                winner_list.append(i[0])
            else:
                continue
        if not winner_list:
            return winner
        else:
            winner_list.append(winner)
            return winner_list

    def white_player(self):
        """Randomly determine which player plays as white among a list of matches"""
        data_match = DataMatch(self.id_tournament)

        match_loaded = data_match.load_match_tournament()

        view_match = ViewMatch(match_loaded)

        view_match.white_player()

    def add_checking(self, id_tournament, match):
        """Add the updated match to the JSON file"""
        data_checking = UpdateMatch(id_tournament)

        update_checking = data_checking.load_checking()

        update_checking.append(match)

        data_checking.checking_match(update_checking)

    def clear_checking(self, id_tournament):
        """Clean up the checking list in the JSON file"""
        data_checking = UpdateMatch(id_tournament)

        clear_checking = data_checking.load_checking()

        clear_checking.clear()

        data_checking.checking_match(clear_checking)

    def checking_loaded(self, id_tournament):
        """Load the JSON file containing the already updated matches and return it as a list"""
        data_checking = UpdateMatch(id_tournament)

        data = data_checking.load_checking()

        return data
