#!/usr/bin/python3
# coding: utf-8

from controllers.player_controller import PlayerController
from views.main_views import MainView
from views.player_view import PlayerView
from views.report_view import ViewReport
from controllers.tournament_controller import TournamentController
from controllers.report_controller import ReportCtrl
from controllers.main_controller import folder_needed


def main():
    folder_needed()
    while True:
        # Displays the main program menu
        user_view = MainView()
        user_choice = user_view.menu()

        # Create tournament
        if user_choice == "t":
            # Request tournament info from user
            orga = user_view.orga_tournament()

            # Builds tournament with user input
            tournois = TournamentController(*orga)
            tournois.create_tournament()

        # Launch tournament
        elif user_choice == "z":
            tournament = TournamentController()
            tournament.run_tournament()

        # Registred players
        elif user_choice == "y":
            # Create an instance of the class to display messages to the user
            user_player = PlayerView()

            # Creates an instance of the class that will manipulate the data
            adding_player = PlayerController()

            while user_choice is not True:
                # Requests player info and saves it
                data_player = user_player.add_player_input()
                adding_player.add_player(data_player)
                user_choice = user_player.other_add()

        # View reports
        elif user_choice == "r":
            view_report = ViewReport()
            report_ctrl = ReportCtrl()

            # Retrieves the value returned by the user
            user_input = view_report.choice_report()

            # Choose the report to generate
            report_ctrl.all_report(user_input)

        # Quit program
        elif user_choice == "x":
            exit()
        else:
            print("Veuillez indiquer une des options proposées.")


if __name__ == "__main__":
    main()
