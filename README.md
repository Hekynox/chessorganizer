
ChessOrganizer
==============

# English version
* [English version of readme](#english)

Description
-----------

ChessOrganizer est un programme logiciel hors ligne visant à être utilisé pour l'organisation et la gestion de tournois d'échecs. 
Il fonctionnera sous Windows, Mac et Linux.

### Tables des matières
* [Liste des fonctionnalités](#fonctionnalités)
* [Installation et utilisation](#installation-utilisation)
* [Créér un tournoi](#créer-un-tournoi)
* [Ajouter des joueurs](#ajouter-des-joueurs)
* [Lancer un tournoi](#lancer-un-tournoi)
* [Consulter des rapports](#consulter-rapports)
* [Lancer un test de conformité du code (flake8)](#flake8)

Fonctionnalités <a name="fonctionnalités"></a>
---------------

### Organisation

Il est possible de créer un tournoi, les informations suivantes seront renseignés pour chaque tournoi étant créé : 

* Nom
* Lieu
* Date de début et de fin
* Nombre de tours - Valeur par défaut = 4
* Numéro correspondant au tour actuel
* Liste des tours
* Liste des joueurs enregistrés
* Description

Chaque joueurs doit être enregistré avec les informations suivantes : 

* Nom de famille
* Prénom
* Date de naissance

### Tours & Matchs

Concernant la gestion des tours et des matchs, chaque tour est nommé tel que ceci (Round 1, Round2 etc...) et est horodaté lors de sa création ainsi que de sa clôture. 

Les joueurs et score de chaque match seront stockés afin de conserver un historique. 

### Déroulement du tournoi

Pour l'établissement de l'ordre de participation, un mélange aléatoire des participants est effectué et chaque tour est généré dynamiquement en fonction des résultats des joueurs en suivant ses règles : 

* Les joueurs sont triés en fonction de leur nombre total de points dans le tournoi
* Associations des joueurs dans l'ordre suivant (J1 vs J2, J3 vs J4 etc...)
* Si deux joueurs possède un nombre de points identiques, le choix se fait aléatoirement
* Le programme évite les matchs doublons, si J1 à déjà affronté J2 au tour 1, J1 ne réaffrontera pas J2 au tour 2 (dans la mesure du possible)
* Les points des joueurs sont mis à jours après chaque match

### Rapports

Il est possible de consulter des rapports qui sont mis à dispositions dans le programme a fin de conserver un historique ainsi que les données pertinentes.
Un rapport sera consultable pour chacune des informations suivantes : 

* Liste de tous les joueurs (triée par ordre alphabétique)
* Liste de tous les tournois
* Consulter un tournoi et ses informations à partir de son nom et sa date
* Liste des joueurs d'un tournoi (triée par ordre alphabétique)
* Liste de tous les tours et matchs d'un tournoi

### Données

Le programme fonctionnant entièrement en état hors ligne, les données sont sauvegardés localement dans des fichier JSON, ces données sont actualisées et récupérer en temps réel afin de permettre un suivi au plus proche du déroulé du tournoi et permet ainsi de sauvegarder et restaurer l'état de celui ci à tout moment, prévenant toutes pertes d'informations.

### Vérification de la conformité du code

Vous pouvez effectuer une vérification de la conformité du code à la norme PEP8 à l'aide du module flake8 qui permettra de générer des rapports contenant les erreurs de conformités.

# Installation et utilisation <a name="installation-utilisation"></a>

Pour utiliser le programme il vous suffit d'importer le projet, créer votre envionnement virtuel et de le lancer comme ceci : 

⚠️ - **Python3.7 ou plus récent est nécessaire pour que le programme fonctionne correctement !**

_Windows_ :

```
git clone https://gitlab.com/Hekynox/ChessOrganizer.git
mkdir data\tournament
python -m venv env
.\env\Scripts\activate
pip install -r requirements.txt
python.exe .\ChessOrganizer.py
```

_Linux_ :

```
git clone https://gitlab.com/Hekynox/ChessOrganizer.git
mkdir -p data/tournament
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
chmod +x ./ChessOrganizer.py
python3 ./ChessOrganizer.py
```

# Utilisation

Une fois le programme lancé, suivez les instructions a fin de choisir l'option que vous souhaitez utiliser. 

## Créer un tournoi <a name="Créer un tournoi"></a>

A l'aide de cette option vous pouvez créer votre tournoi, lors de la création de celui ci vous serez invité à entrer les informations nécessaire à sa création. 

Une fois le tournoi créé celui ci obtiendra un ID d'identification en même temps que d'être enregistré dans un fichier JSON contenant tout les tournois créé, cette ID d'identification sera affiché dans le terminal pour vous indiquer l'ID du tournoi que vous venez de créer. 

Les ID d'identification des tournois sont procédés comme suit : 
Le 1er tournoi portera l'ID 001, les suivant seront incrémentés par rapport au tournoi précédent (Le 2e tournoi portera donc l'ID 002 et ainsi de suite).

## Ajouter des joueurs <a name="Ajouter des joueurs"></a>

Vous pourrez ajouter des joueurs à un tournoi par le biais de cette option, il vous suffit de simplement vous munir de l'ID d'identification du tournoi a fin de pouvoir indiquer à quel tournoi vous souhaitez inscrire les joueurs que vous allez ajouter. 

Quand un joueur est enregistré dans un tournoi, il est également enregistré automatiquement dans le fichier JSON répertoriant tous les joueurs ainsi que bien evidemment dans la liste des joueurs du tournoi associé. 

## Lancer un tournoi <a name="Lancer un tournoi"></a>

Encore une fois, il sera nécessaire de connaître l'ID d'identification du tournoi a fin de pouvoir sélectionner le tournoi à lancer.

Attention, il est nécessaire que le nombre de joueur du tournoi soit pair pour pouvoir être lancé, ceci pour des raisons d'équité au cours de celui ci.

Une fois le tournoi lancé, celui ci se déroulera comme indiqué dans les fonctionnalités, à chaque round, une liste des matchs du round s'affichera et vous demandera d'entrer le numéro du matchs dont vous souhaitez entrer le résultat pour le round en cour, une fois que vous avez entré les résultats de tous les matchs du round en cours, le programme passe au round suivant et répètera l'opération jusqu'à que le tournoi se termine. 

## Consulter les rapports <a name="consulter-rapports"></a>

Vous pouvez consulter des rapports qui seront généré en fichier .html pour une meilleure lisibilité.

Voici les rapports pouvant être généré : 

### Liste de tous les joueurs par ordre alphabétique

- Générera un rapport contenant la liste de tous les joueurs enregistrés trié par ordre alphabétique via le Nom. 

### Liste de tous les tournois

- Générera un rapport listant tous les ID des tournois. 

### Nom et date d'un tournoi donné

- Générera un rapport en fonction de l'ID du tournoi renseigné, celui ci contiendra le nom, la date de début et la date de fin du tournoi. 

### Liste des joueurs du tournoi par ordre alphabétique

- Générera un rapport en fonction de l'ID du tournoi renseigné, celui ci contiendra la liste des joueurs du tournoi trié par ordre alphabétique.

### Liste de tous les tours du tournoi et de tous les matchs du tour

- Générera un rapport en fonction de l'ID du tournoi renseigné, celui ci contiendra les rounds du tournoi ainsi que les matchs ayant été disputé pour chacun de ses tours. 

## Ou retrouver les rapports générés ? 

Les rapports sont générés dans le dossier **report/** sous format .html, pour les consulter il vous suffit de double cliquer dessus pour les ouvrir avec votre navigateur préféré ! 

## Lancer un test flake8 et générer des rapports d'erreurs <a name="flake8"></a>

Si vous souhaitez lancer un test de la conformité du code, il vous suffira (après avoir suivi les étapes [Installation et utilisation](#installation-utilisation)) de lancer le script nommé **run_flake8.py** 

Une fois celui ci lancé, il va vérifier tous les fichiers .py nécessaire au fonctionnement du programme et créer des rapports contenant les erreurs si il en trouve. 

Ces rapports sont générés en .html, ils seront donc consultable à l'aide de votre navigateur préféré. 

Pour les consulter, rien de plus simple, à partir de la racine du projet, selectionné le dossier nommé **flake8-report** , il contient tous les rapports qui seront générés par flake8.

English Version <a name="english"></a>
======================================

Description
-----------

ChessOrganizer is an offline software program designed for organizing and managing chess tournaments. It will work on Windows, Mac, and Linux.

### Table of Contents

* [Features](#features)
* [Installation and Usage](#installation-utilisation-en)
* [Creating a Tournament](#creating-tournament)
* [Adding players](#adding-players)
* [Starting a Tournament](#starting-tournament)
* [Viewing Reports](#viewing-reports)
* [Running Code Compliance Test (flake8)](#flake8-en)

Features <a name="features"></a>
---------------------------------------

### Organization

It is possible to create a tournament, and the following information will be provided for each created tournament: 

* Name
* Location
* Start and end date
* Number of rounds - Default value = 4
* Number corresponding to the current round
* List of rounds
* List of registred players
* Description

Each player must be registered with the following information:

* Last Name
* First Name
* Date of birth

### Rounds & Matches

Regarding the management of rounds and matches, each round is named as follows (Round 1, Round2, etc.) and is timestamped upon creation as well as closure.

The players and scores of each match will be stored to maintain a history.

### Tournament Progression

For the establishment of the order of participation, a random shuffle of the participants is performed, and each round is dynamically generated based on the results of the players following these rules:

* Players are sorted by their total number of points in the tournament.
* Players are matched in the following order (P1 vs P2, P3 vs P4, etc.).
* If two players have the same number of points, the choice is made randomly.
* The program avoids duplicate matches; if P1 has already faced P2 in round 1, P1 will not face P2 again in round 2 (as far as possible).
* Players' points are updated after each match.

### Reports

It is possible to consult reports that are available in the program to maintain a history as well as relevant data. A report will be available for each of the following:

* List of all players (sorted alphabetically)
* List of all tournaments
* View a tournament and its information by name and date
* List of players in a tournament (sorted alphabetically)
* List of all rounds and matchs in tournament

### Data

Since the program operates entirely offline, the data is saved locally in JSON files. These data are updated and retrieved in real-time to allow for close monitoring of the tournament's progress and to save and restore its state at any time, preventing any loss of information.

### Code Compliance Check

You can perform a code compliance check to the PEP8 standard using the flake8 module, which will generate reports containing compliance errors.

# Installation and Usage <a name="installation-utilisation-en"></a>

For using this program, simply import the project, create your virtual environment, and launch it as follows: 

⚠️ - **Python3.7 or newer is strictly necessary for using this program !**

_Windows_ :

```
git clone https://gitlab.com/Hekynox/ChessOrganizer.git
mkdir data\tournament
python -m venv env
.\env\Scripts\activate
pip install -r requirements.txt
python.exe .\ChessOrganizer.py
```

_Linux_ :

```
git clone https://gitlab.com/Hekynox/ChessOrganizer.git
mkdir -p data/tournament
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
chmod +x ./ChessOrganizer.py
python3 ./ChessOrganizer.py
```

# Usage

Once the program is launched, follow the instructions to choose the option you want to use.

## Creating a Tournament <a name="creating-tournament"></a>

Using this option, you can create your tournament. During the creation of the tournament, you will be prompted to enter the necessary information.

Once the tournament is created, it will be assigned an identification ID while being saved in a JSON file containing all the created tournaments. This identification ID will be displayed in the terminal to indicate the ID of the tournament you just created.

The identification IDs of the tournaments are proceeded as follows: The first tournament will have ID 001, and subsequent ones will be incremented based on the previous tournament (the second tournament will have ID 002, and so on).

## Adding players <a name="adding-players"></a>

You can add players to a tournament through this option. Simply provide the identification ID of the tournament to which you want to add the players.

When a player is registered in a tournament, they are also automatically registered in the JSON file listing all players, as well as in the list of players for the associated tournament.

## Starting a Tournament <a name="starting-tournament"></a>

Again, you will need to know the identification ID of the tournament to select the tournament to start.

Please note that the number of players in the tournament must be even in order to be launched, for reasons of fairness during the tournament.

Once the tournament is launched, it will proceed as indicated in the features. At each round, a list of matches for the round will be displayed, and you will be prompted to enter the match number for which you want to enter the result for the current round. Once you have entered the results of all the matches for the current round, the program moves on to the next round and repeats the process until the tournament is completed.

## Viewing Reports <a name="viewing-reports"></a>

You can consult reports generated in .html format for better readability.

Here are the reports that can be generated:

### List of All players in alphabetical order

* Generates a report containing the list of all registered players sorted alphabetically by last name.

### List of all tournaments

* Generates a report listing all tournament IDs

### Name and date of a given tournament

* Generates a report based on the provided tournament ID. This report will contain the name, start date, and end date of the tournament.

### List of players in the tournament in alphabetical order

* Generates a report based on the provided tournament ID. This report will contain the list of players in the tournament sorted alphabetically.

### List of all round in the tournament and all matchs in the round

* Generates a report based on the provided tournament ID. This report will contain the rounds of the tournament as well as the matches that have been played for each round.

## Where to find generated reports ? 

Reports are generated in the **report/** folder in .html format. To view them, simply double click on them to open them with your preferred browser!

## Running a flake8 test and generating error reports <a name="flake8-en"></a>

If you want to run a code compliance check, you can do so by following these steps (after following the [Installation and Usage](#installation-utilisation-en) steps). Simply run the script named **run_flake8.py**.

Once this is done, it will check all .py files necessary for the program's operation and create reports containing any compliance errors found.

These reports are generated in .html format and can be viewed with your preferred browser.

To view them, simply navigate to the root of the project and select the folder named flake8-report. It contains all the reports that will be generated by