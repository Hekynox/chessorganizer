import json
import os


class DataTournament:
    def __init__(self, id_tournament=""):
        """Initalizes the path used to access tournament data and the tournament id if specified"""
        self.path_list_tournament = "data/tournament/tournament_list.json"
        self.id = id_tournament

    def save_new_tournament(self, data):
        """Save tournament data in a JSON file and create a folder with the
        corresponding tournament id to store future tournament data (matches, score)."""

        # Loads the tournament data file if it exists
        data_load = self.load_file_tournament()

        if data_load:
            # Transforms the dictionary into a list to retrieve keys
            keys_list = list(data_load.keys())

            last_key = keys_list[-1]

            # Retrieves and converts the id of the last dictionary key to integer
            last_id = last_key[-3:]
            last_id = int(last_id)

            # Create id for next tournament (Tournament1 = id_002 -> Tournament2 = id_003)
            new_last_id = last_id + 1
            new_last_id_str = str(new_last_id).zfill(3)
            new_id = "id_" + new_last_id_str

            # Creates and adds new tournament dictionary to tournament data
            data_load[new_id] = data

            # Create the corresponding tournament folder containing the rounds
            os.makedirs(f"data/tournament/tournament_{new_last_id_str}")

            # Saves changes to the tournament file
            self.save_file_tournament(data_load)
            return new_last_id_str
        else:
            # Create dictionnary
            data_load = {}

            # Creates and adds the new tournament dictionary to the previously created dictionary
            data_load["id_001"] = data

            # Create the tournament_001 folder containing the rounds
            os.makedirs("data/tournament/tournament_001")

            # Saves tournament data in a JSON file
            self.save_file_tournament(data_load)
            return "001"

    def load_file_tournament(self):
        """Loads and returns the JSON file containing the tournaments"""
        if os.path.exists(self.path_list_tournament):
            with open(self.path_list_tournament, "r") as file:
                data_load = json.load(file)
                return data_load

    def save_file_tournament(self, data):
        """Saves the data in the JSON file containing the tournaments"""
        with open(self.path_list_tournament, "w") as file:
            json.dump(data, file, indent=1)

    def load_tournament(self, id_tournament=""):
        """Find and load tournament data with the specified id"""
        if id_tournament:
            self.id = id_tournament
        if os.path.exists(self.path_list_tournament):
            with open(self.path_list_tournament, "r") as file:
                data_load = json.load(file)
                if f"id_{self.id}" in data_load:
                    data_tournament = data_load[f"id_{self.id}"]
                    return data_tournament
                else:
                    return False
        else:
            file_not_found = "file not found"
            return file_not_found

    def save_tournament(self, data):
        """Saves tournament data in a json file"""
        load_data = self.load_file_tournament()
        load_data[f"id_{self.id}"] = data

        with open(self.path_list_tournament, "w") as file:
            json.dump(load_data, file, indent=1)
