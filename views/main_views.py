class MainView:
    def __init__(self):
        pass

    def menu(self):
        """Display main menu of program to interact with user"""
        print()
        print("**MENU**")
        print("t = créer un tournoi")
        print("z = lancer un tournoi")
        print("y = ajouter des joueurs")
        print("r = consulter les rapports")
        print("x = quitter le programme")
        print()
        user_choice = input(": ")
        return user_choice

    def orga_tournament(self):
        """Displays tournament properties to be entered by the user"""
        name = input("Nom du tournoi : ")
        location = input("Lieu : ")
        start_date = input("Date de debut : ")
        end_date = input("Date de fin : ")
        listed_player = []
        currently_round = 1
        verif = False
        while verif is False:
            round_nbr = input("Nombre de round du tournoi : ")
            if round_nbr.isdigit():
                round_nbr = int(round_nbr)
                verif = True
            else:
                print("Il faut entrer un chiffre !")
        return name, location, start_date, end_date, listed_player, currently_round, round_nbr

    def choice_tournament(self):
        """asks the user which tournament to launch"""
        print()
        user_choice = input("Entrez l'id du tournoi à lancer (ex : 002) : ")
        return user_choice
