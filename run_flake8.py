#!/usr/bin/python3
# coding: utf-8

import subprocess
import os


def main():
    if os.path.exists("flake8-report/"):
        pass
    else:
        os.mkdir("flake8-report/")

    subprocess.run(f"flake8 --format=html --htmldir=flake8-report --max-line-length=119 --exclude=__pycache__,__init__.py,env/,run_flake8.py")

if __name__ == "__main__":
    main()