from models.player import DataPlayer
from views.player_view import PlayerView


class PlayerController:
    def __init__(self):
        pass

    def add_player(self, data_player):
        """Add the player to the database"""
        # Create an instance of the class that will manage player data
        player_tournament = DataPlayer()

        # Create an instance of the class that manages the display
        add_player_view = PlayerView()

        # Initialize the list that contains all the players
        list_all_player = []

        # Create a dictionary containing the player's information
        player = {"Prenom": data_player[0],
                  "Nom": data_player[1],
                  "Date de naissance": data_player[2],
                  "Identifiant national d'echecs": data_player[3]}

        # Adds player to tournament and does nothing and returns False if no file/tournament found
        player_add_tournament = player_tournament.add_player_tournament(data_player)

        if isinstance(player_add_tournament, tuple):
            add_tournament = player_add_tournament[0]
            idstr = player_add_tournament[1]
        else:
            add_tournament = player_add_tournament

        if add_tournament is not False:
            if not player_tournament.already_exist(player):
                list_all_player.append(player)
                player_tournament.add_list_all_players(list_all_player)
                add_player_view.success_add_player()
            else:
                add_player_view.fail_to_add()
        else:
            add_player_view.add_tournament_fail(idstr)
