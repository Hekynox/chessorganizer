class ViewReport:
    def __init__(self):
        pass

    def choice_report(self):
        """Displays proposed reports for consultation"""
        input_tupl = ("1", "2", "3", "4", "5")

        verif = False
        while verif is False:
            print("Quel rapport souhaitez vous consulter ? ")
            print("")
            print("** 1 : Liste de tous les joueurs **")
            print("** 2 : Liste des tournois **")
            print("** 3 : Nom et date d'un tournoi **")
            print("** 4 : Liste des joueurs d'un tournoi **")
            print("** 5 : Liste de tous les round et matchs des round du tournoi")
            input_user = input(" : ")
            if input_user not in input_tupl:
                print("Choisissez une des options proposées !")
            else:
                verif = True
        return input_user

    def choice_tournament(self):
        """Asks the user for the ID of the tournament for which information is required"""
        print("Entrer l'ID du tournoi : ")
        input_user = input(" : ")
        return input_user

    def html_all_players(self):
        """Template of the HTML file generated for the report of all players"""

        template = """
        <html>
        <head><title>ChessOrganizer</title></head>
        <style>
        .red-text{color: red;}
        </style>
        <body>
        <h1>Liste de tous les joueurs</h1>
        <d1>
        {% for i in sorted_data %}
            {% for key, value in i.items() %}
                <dt><span class="red-text">{{ key|e}}</span></dt>
                <dd>{{value|e}}</dd>
            {% endfor %}
            <br>
        {% endfor %}
        </d1>
        </body>
        </html>
        """

        return template

    def html_list_tournament(self):
        """Template for the HTML file generated for the tournament list report"""
        template = """
        <html>
        <head><title>ChessOrganizer</title></head>
        <style>
        .red-text{color: red;}
        </style>
        <body>
        <h1>Liste des tournois</h1>
        <ul>
        {% for i in data %}
            <li><span class="red-text">Tournoi ID : </span>{{ i|e}}</li>
            <br>
        {% endfor %}
        </ul>
        </body>
        </html>
        """

        return template

    def html_info_tournament(self):
        """Template for the HTML file generated for the name and date of tournament"""
        template = """
        <html>
        <head><title>ChessOrganizer</title></head>
        <style>
        .red-text{color: red;}
        </style>
        <body>
        <h1>Nom et date de début et de fin du tournoi : {{id_tournament}}</h1>
        <ul>
        <li><span class="red-text">Nom du tournoi :</span> {{name}}</li>
        <li><span class="red-text">Date de début :</span> {{start_date}}</li>
        <li><span class="red-text">Date de fin :</span> {{end_date}}</li>
        </ul>
        </body>
        </html>
        """

        return template

    def html_list_players(self):
        """Template for the HTML file generated for list of player in tournament"""
        template = """
        <html>
        <head><title>ChessOrganizer</title></head>
        <style>
        .red-text{color: red;}
        </style>
        <body>
        <h1>Liste des joueurs du tournoi ID : {{id_tournament}}</h1>
        <ul>
        {% for i in list_players %}
            <li><span class="red-text">Joueur {{loop.index}} : </span>{{ i|e}}</li>
            <br>
        {% endfor %}
        </ul>
        </body>
        </html>
        """

        return template

    def html_round_matchs(self):
        """Template for the HTML file generated for list of round an match in tournament"""
        template = """
        <html>
        <head><title>ChessOrganizer</title></head>
        <style>
        .red-text{color: red;}
        .green-text{color: green;}
        </style>
        <body>
        <h1>Liste des rounds et des matchs du tournoi ID : {{id_tournament}}</h1>
        <d1>
        {% for key, value in data.items() %}
            <dt><b><span class="red-text">{{key|e}}</span></b></dt>
            <br>
            {% for key1, value1 in value.items() %}
                <dt><span class="green-text">{{key1|e}} : </span></dt>
                <dd><b>{{value1|e}}</b></dd>
            {% endfor %}
            <br>
        {% endfor %}
        </d1>
        </body>
        </html>
        """

        return template
