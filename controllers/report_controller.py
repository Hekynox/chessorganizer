from models.report import Report
from views.report_view import ViewReport
from views.tournament_view import ViewTournament
from models.player import DataPlayer
from models.tournament import DataTournament
from models.matchmaking import DataMatch


class ReportCtrl:
    def __init__(self):
        self.report = Report()
        self.view = ViewReport()
        self.data = DataPlayer()
        self.tournament = DataTournament()
        self.view_tournament = ViewTournament()

    def generate_all_players_report(self):
        """Generates HTML reports for all players"""

        # Retrieves data from JSON file listing all players
        all_players = self.data.load_all_players()

        # Retrieves HTML template to list players
        template = self.view.html_all_players()

        # Calls a function to generate the HTML file
        self.report.report_all_players(template, all_players)

    def tournament_report(self):
        """Generates an HTML report of all tournaments"""

        # Load file of tournaments
        tournament = self.tournament.load_file_tournament()

        # Generates a list of tournament IDs
        list_of_key = self.extract_id_tournament(tournament)

        # Retrieves HTML template to list tournaments
        template = self.view.html_list_tournament()

        # Calls a function to generate the HTML file
        self.report.report_all_tournaments(template, list_of_key)

    def tournament_info_report(self, id_tournament):
        """Generates an HTML report containing the name and date of a given tournament"""

        # Loads tournament data with ID matching
        data_tournament = self.verify_id(id_tournament)

        if data_tournament is False:
            self.view_tournament.tournament_not_exist()
        else:
            # Retrieves tournament name and start/end date
            list_of_data = self.get_info_tournament(data_tournament)

            # Retrieves HTML template for tournament info
            template = self.view.html_info_tournament()

            # Calls a function to generate the HTML file
            self.report.report_info_tournament(template, list_of_data, id_tournament)

    def players_tournament(self, id_tournament):
        """Generates an HTML report containing an alphabetically sorted
        list of players in a given tournament"""

        # Loads tournament data with id matching
        data_tournament = self.verify_id(id_tournament)

        if data_tournament is False:
            self.view_tournament.tournament_not_exist()
        else:
            # Retrieves the tournament player list
            list_players = self.get_players(data_tournament)

            # Retrieves HTML template to list tournament players
            template = self.view.html_list_players()

            # Calls a function to generate the HTML file
            self.report.report_list_players(template, list_players, id_tournament)

    def matchs_tournament(self, id_tournament):
        """Generates an HTML report containing a list of rounds and matches for each round"""

        data_matchs = DataMatch(id_tournament)

        # Loads tournament round and match data
        data_tournament = self.verify_id(id_tournament)

        # recovers tournament rounds and matches
        data_round = data_matchs.load_match_tournament()

        if data_tournament is False:
            self.view_tournament.tournament_not_exist()
        else:
            # Retrieves HTML template to list tournament rounds and matches
            template = self.view.html_round_matchs()

            # Calls a function to generate the HTML file
            self.report.report_rounds_matchs(template, data_round, id_tournament)

    def verify_id(self, id_tournament):
        """Checks if tournament id exists"""
        data_tournament = self.tournament.load_tournament(id_tournament)
        return data_tournament

    def extract_id_tournament(self, file):
        """Extract tournament IDs and add them to a list"""

        list_of_key = []

        for key, value in file.items():
            new_key = key.split("_")[1]
            list_of_key.append(new_key)

        return list_of_key

    def get_info_tournament(self, data):
        """Retrieves the name and date of the loaded tournament"""
        list_of_data = []
        list_of_data.append(data[0])
        list_of_data.append(data[2])
        list_of_data.append(data[3])
        return list_of_data

    def get_players(self, data):
        """Retrieves a tournament's player list and sorts it alphabetically"""
        list_players = []
        players = data[4]
        for i in players:
            list_players.append(i)
        list_players_sorted = sorted(list_players, key=lambda x: x.split()[1])
        return list_players_sorted

    def all_report(self, user_choice):
        """Acts according to the report to be generated"""

        # if "1" : Generates a report listing all players
        if user_choice == "1":
            self.generate_all_players_report()
        # if "2" : Generates a report containing a list of tournaments
        elif user_choice == "2":
            self.tournament_report()
        # if "3" : Generates a report containing the name and date of a given tournament
        elif user_choice == "3":
            user_input = self.view.choice_tournament()
            self.tournament_info_report(user_input)
        # if "4" : Generates a report containing a list of tournament players in alphabetical order
        elif user_choice == "4":
            user_input = self.view.choice_tournament()
            self.players_tournament(user_input)
        # if "5" : Generates a report containing a list of all rounds in the tournament and all matches in the round
        elif user_choice == "5":
            user_input = self.view.choice_tournament()
            self.matchs_tournament(user_input)
