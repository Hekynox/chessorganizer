from jinja2 import Template


class Report:
    def __init__(self):
        """Initialize path of folder containing report and name of file"""
        self.report_path = "report/"
        self.all_players_file = "report_all_players.html"
        self.tournament_file = "report_all_tournament.html"

    def report_all_players(self, template, data):
        """Generates an HTML report of the list of all players"""

        template_ok = Template(template)

        # Alphabetical sorting of dictionaries containing player data
        sorted_data = sorted(data, key=lambda x: x["Nom"].lower())

        # Generates HTML report using template and data
        html_all_players = template_ok.render(sorted_data=sorted_data)

        # Create an .html file containing the generated html report
        self.generate_report(self.all_players_file, html_all_players)

    def report_all_tournaments(self, template, data):
        """Generates an HTML report of the tournament list"""
        template_ok = Template(template)

        # Generates HTML report using template and data
        html_list_tournament = template_ok.render(data=data)

        # Create an .html file containing the list of tournaments
        self.generate_report(self.tournament_file, html_list_tournament)

    def report_info_tournament(self, template, data, id_tournament):
        """Generates an HTML report containing the name and start/end date of a tournament"""
        template_ok = Template(template)

        tournament_info_file = f"report_tournamentID_{id_tournament}.html"

        # Retrieves name, start date, end date from variables
        name = data[0]
        start_date = data[1]
        end_date = data[2]

        # Generates HTML report using template and data
        html_info_tournament = template_ok.render(name=name,
                                                  start_date=start_date,
                                                  end_date=end_date,
                                                  id_tournament=id_tournament)

        # Create an .html file containing tournament info
        self.generate_report(tournament_info_file, html_info_tournament)

    def report_list_players(self, template, data, id_tournament):
        """Generates an HTML report containing the list of players in a tournament"""
        template_ok = Template(template)

        list_players_file = f"report_listed_players_tournamentID_{id_tournament}.html"

        # Generates HTML report using template and data
        html_players_tournament = template_ok.render(list_players=data,
                                                     id_tournament=id_tournament)

        # Create an .html file containing the list of tournament players
        self.generate_report(list_players_file, html_players_tournament)

    def report_rounds_matchs(self, template, data, id_tournament):
        """Generates an HTML report containing tournament rounds and matches"""

        template_ok = Template(template)

        round_matchs_file = f"report_round_and_matchs_tournamentID_{id_tournament}.html"

        # Generates HTML report using template and data
        html_round_matchs = template_ok.render(data=data,
                                               id_tournament=id_tournament)

        # Create an .html file containing the tournament rounds and matches
        self.generate_report(round_matchs_file, html_round_matchs)

    def generate_report(self, path_file, data):
        """Create file report with name of file and data given"""
        with open(self.report_path + path_file, "w") as f:
            f.write(data)
