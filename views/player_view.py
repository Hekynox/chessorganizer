import re


class PlayerView:
    def __init__(self):
        pass

    def add_player_input(self):
        """Recovers user entries"""
        first_name = input("prenom : ")
        last_name = input("nom de famille : ")
        birth_date = input("date de naissance : ")
        verif = False
        while verif is False:
            ind = input("identifiant national d'echecs (club) : ")

            # Regex to check whether the IND format is correct
            regex = r"^[a-zA-Z]{2}\d{5}$"
            if re.match(regex, ind):
                verif = True
            else:
                print("Identifiant nationale d'échecs non valide")
                print("Celui ci doit contenir 2 lettres suivi de 5 chiffres")
                print("Exemple : ab12345")
        print("A quel tournoi inscrire le joueur ?")
        id_tournament = input("Entrer l'id du tournoi (ex : 001): ")
        return first_name, last_name, birth_date, ind, id_tournament

    def success_add_player(self):
        """Display a message if the player has been added successfully"""
        print("Nouveau joueur ajouté a la base de données avec succès !")

    def fail_to_add(self):
        """Display a message if the player could not be added"""
        print("Le joueur existe déjà dans la base de données")
        print("Enregistrement annulé")

    def add_tournament_fail(self, idstr):
        """Display a message if adding the player to the tournament failed"""
        print(f"Le tournoi '{idstr}' n'as pas été trouvé")
        print("le joueur n'a pas été enregistré")
        print("l'id du tournoi indiqué n'est pas valide")
        print("")
        print("veuillez recommencer l'operation avec un id tournoi valide")
        print("!! Assurez vous que vous avez bien crée un tournoi avant !!")

    def other_add(self):
        """Asks the user if he wants to add another player"""

        user_entry = ""
        while user_entry != "y" and user_entry != "n":
            user_entry = input("Ajouter un autre joueur ? y/n : ")
            if user_entry == "y":
                user_choice = False
                return user_choice
            elif user_entry == "n":
                user_choice = True
                return user_choice
            else:
                print("Veuillez indiquer 'y' (yes/oui) ou 'n' (no/non)")
