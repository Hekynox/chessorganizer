import json
import os


class DataPlayer:
    """Manipulates player data"""
    def __init__(self):
        """Initializes the path of the file to which data is added"""
        self.path_all_players = "data/Players.json"
        self.list_all_players = []
        self.path_tournament = "data/tournament/tournament_list.json"

    def already_exist(self, player_dict):
        """Checks that a player is not already present in the reference
        database of all players"""
        self.load_all_players()
        if self.list_all_players:
            for player in self.list_all_players:
                if (player["Prenom"] == player_dict["Prenom"] and
                    player["Nom"] == player_dict["Nom"] and
                    player["Date de naissance"] == player_dict["Date de naissance"] and
                        player["Identifiant national d'echecs"] == player_dict["Identifiant national d'echecs"]):
                    return True
        else:
            return False

    def load_all_players(self):
        """Loads data from JSON file containing all players"""
        if os.path.exists(self.path_all_players):
            with open(self.path_all_players, "r") as file:
                self.list_all_players = json.load(file)
                return self.list_all_players

    def add_list_all_players(self, list_all_player):
        """Written in the JSON file that serves as the player database"""
        if os.path.exists(self.path_all_players):
            with open(self.path_all_players, "r") as file:
                player = json.load(file)
        else:
            player = []
        if list_all_player[-1] not in player:
            player.append(list_all_player[-1])
        sorted_player = sorted(player, key=lambda x: x["Nom"])
        with open(self.path_all_players, "w") as json_file:
            json.dump(sorted_player, json_file, indent=4)

    def add_player_tournament(self, player_data):
        """Add player to tournament"""

        # Loads the tournament file
        if os.path.exists(self.path_tournament):
            with open(self.path_tournament, "r") as file:
                data_tournament = json.load(file)
        else:
            data_tournament = {}

        idstr = str(player_data[4])

        # Formats tournament id entry (Ex: 001 -> id_001)
        id_tournoi = "id_" + idstr

        # Formats first name and last name into a single string
        player_name = f"{player_data[0]} {player_data[1]}"

        if data_tournament:
            if id_tournoi in data_tournament:
                data_tournament[id_tournoi][4].append(player_name)
                with open(self.path_tournament, "w") as save_file:
                    json.dump(data_tournament, save_file, indent=4)
            else:
                return False, idstr
        else:
            return False
