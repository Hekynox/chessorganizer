import json
from models.data import os


class DataMatch:
    def __init__(self, id_tournament, currently_round=int, list_players_match=[]):
        """Initializes tournament id, current round and player list"""
        self.path_match = f"data/tournament/tournament_{id_tournament}/matchs.json"
        self.currently_round = currently_round
        self.list_players = list_players_match

    def save_match(self, start_time="", end_time=""):
        """Save match JSON file"""
        data_round = self.load_match_tournament()
        if f"Round {self.currently_round}" not in data_round:
            data_round[f"Round {self.currently_round}"] = {}
            indice_match = 1
            if start_time:
                data_round[f"Round {self.currently_round}"]["Date/Heure debut "] = start_time
                data_round[f"Round {self.currently_round}"]["Date/Heure fin "] = ""
            else:
                pass
            for i in self.list_players:
                data_round[f"Round {self.currently_round}"][f"match {indice_match}"] = i
                indice_match += 1

            with open(self.path_match, "w") as file:
                json.dump(data_round, file, indent=4)

    def end_time(self, endtime):
        """Adds round end time"""
        data = self.load_match_tournament()

        data[f"Round {self.currently_round}"]["Date/Heure fin "] = endtime

        with open(self.path_match, "w") as file:
            json.dump(data, file, indent=4)

    def load_match_tournament(self):
        """Load match JSON file"""
        if os.path.exists(self.path_match):
            with open(self.path_match, "r") as file:
                data_round = json.load(file)
                return data_round
        else:
            data_round = {}
            return data_round

    def verify_round(self):
        """Checks whether matches for the current round already exist"""
        round_load = self.load_match_tournament()

        list_of_round = []
        name_round = f"Round {self.currently_round}"

        for key, value in round_load.items():
            list_of_round.append(key)
        if name_round in list_of_round:
            return True
        else:
            return False


class UpdateMatch(DataMatch):
    """Match update"""
    def __init__(self, id_tournament):
        super().__init__(id_tournament, None, None)
        self.path_checking = "checking/checking_match.json"

    def update_match(self, match):
        """Save JSON match file"""
        with open(self.path_match, "w") as file:
            json.dump(match, file, indent=4)

    def load_match_tournament(self):
        """Load match JSON file"""
        return super().load_match_tournament()

    def checking_match(self, data):
        """Saves the JSON file containing the round matches already updated"""
        with open(self.path_checking, "w") as f:
            json.dump(data, f, indent=1)

    def load_checking(self):
        """Loads the JSON file containing the round matches already updated"""
        if os.path.exists(self.path_checking):
            with open(self.path_checking, "r") as f:
                data = json.load(f)
                return data
        else:
            data = []
            return data
