class ViewTournament:
    def __init__(self) -> None:
        pass

    def tournament_created(self, id):
        """Display a message announcing successful tournament registration"""
        print(f"l'id du tournoi que vous venez de créer est : {id}")

    def choice_tournament(self):
        """Requests the ID of the tournament to be launched and returns it"""
        print()
        user_choice = input("Entrez l'id du tournoi à lancer (ex : 002) : ")
        return user_choice

    def info_tournament(self, name, location, start_date, end_date, round_number, players):
        """View tournament information"""
        print()
        print("**Voici les informations de ce tournoi :**")
        print("nom du tournoi :", name)
        print("Lieu : ", location)
        print("Date de debut : ", start_date)
        print("Date de fin : ", end_date)
        print("Nombre de tour : ", round_number)
        print("Participant : ")
        for i in players:
            print(f"- {i}")

    def round_progress(self, currently_round):
        """Displays the current round"""
        print(f"**Début du Round {currently_round} !**")

    def end_tournament(self, winner):
        """Displays a message announcing the end of the tournament"""
        print("** Fin du tournoi !!**")
        print("")
        if isinstance(winner, list):
            print("Les gagnants du tournoi sont : ")
            for i in winner:
                print(f"- {i} ! Bravo !")
        else:
            print(f"Le gagnant du tournoi est {winner} ! Félicitation !")

    def tournament_not_exist(self):
        """Display a message if the tournament does not exist"""
        print("L'id du tournoi que vous avez entré n'existe pas")
        print("Merci de rentrer l'id d'un tournoi existant")

    def file_not_found(self):
        """Display a message if the tournament file cannot be found"""
        print("Aucun fichier des tournois trouvé")

    def not_peer(self):
        """Display a message indicating that the number of players is not even"""
        print("Le nombre de joueur du tournoi n'est pas pair")
        print("")
        print("Il est nécessaire que le nombre de joueur du tournoi soit pair")
        print("Ajouter un joueur supplémentaire au tournoi a fin de pouvoir le lancer")
