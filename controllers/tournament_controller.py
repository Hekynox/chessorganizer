from models.tournament import DataTournament
from views.tournament_view import ViewTournament
from controllers.match_controller import Matchmaking, DataMatch
import math
import random
from datetime import datetime


class TournamentController:
    def __init__(self, name="", location="", start_date="", end_date="", listed_player=[],
                 currently_round=1, round_number=4, description="", id_tournament=""):
        """Initialize tournament structure and ID"""
        self.name = name
        self.location = location
        self.start_date = start_date
        self.end_date = end_date
        self.currently_round = currently_round
        self.round_number = round_number
        self.description = description
        self.listed_player = listed_player
        self.id = id_tournament

    def create_tournament(self):
        """Create tournament"""
        data_tournament = DataTournament()
        view_tournament = ViewTournament()

        # Retrieves tournament data in list format
        data_new_tournament = self.get_data_tournament()

        # Saves the tournament in a JSON file
        new_tournament = data_tournament.save_new_tournament(data_new_tournament)

        # Returns that the tournament has been created and its ID
        view_tournament.tournament_created(new_tournament)

    def get_data_tournament(self):
        """Renvoi que le tournoi a été créé et son ID"""
        tournament = []
        tournament.append(self.name)
        tournament.append(self.location)
        tournament.append(self.start_date)
        tournament.append(self.end_date)
        tournament.append(self.listed_player)
        tournament.append(self.currently_round)
        tournament.append(self.round_number)
        tournament.append(self.description)
        return tournament

    def run_tournament(self):
        """Manages the running of the tournament"""
        view_tournament = ViewTournament()

        # Contains False and will contain True if the tournament is over
        end_tournament = False

        # Contains False and will contain True if the tournament exists
        verif_tournament = False

        # Loops until an existing tournament is entered
        while verif_tournament is False:
            # Requests and stores the ID of the tournament to be launched for the user
            tournament_choice = view_tournament.choice_tournament()

            # Create an instancer to manage tournament data
            data_tournament = DataTournament(tournament_choice)

            # Loads tournament data with id matching
            loading_tournament = data_tournament.load_tournament()

            if loading_tournament is False:
                view_tournament.tournament_not_exist()
            elif loading_tournament == "file not found":
                view_tournament.file_not_found()
            else:
                verif_tournament = True

        # Load the tournament into an instance
        loaded_tournament = TournamentController(*loading_tournament)

        # Checks whether the number of players is even or not
        if self.peer_players(loading_tournament[4]) is False:
            return view_tournament.not_peer()

        # Retrieves items from the tournament for display to the user
        recover_info = loaded_tournament.recover_info()
        view_tournament.info_tournament(*recover_info)

        # Tournament main loop (stops when tournament over)
        while end_tournament is False:
            tournoi_loaded = data_tournament.load_tournament()
            loaded_tournament = TournamentController(*tournoi_loaded, tournament_choice)

            # If tournament is round 1
            if loaded_tournament.currently_round == 1:
                # Randomly shuffle the player list
                loaded_tournament.random_mixed()

                # Returns a tuple list containing matches without scores
                match_round = loaded_tournament.established_first_round()

                # Create and save the 1st round match list
                matchmaking = Matchmaking(match_round, tournament_choice, loaded_tournament.currently_round)
                matchmaking.match_first_round()

                # Choose which player will play white
                matchmaking.white_player()

                # Asks user to enter 1st round results
                matchmaking.select_match(tournament_choice)

                # Ends the round and increments the current round by 1
                loaded_tournament.end_round(tournament_choice)
                matchmaking.clear_checking(tournament_choice)

            # If the tournament is in round > 1
            elif loaded_tournament.currently_round > 1:
                match_organize = Matchmaking(loaded_tournament.listed_player,
                                             tournament_choice,
                                             loaded_tournament.currently_round)
                view_tournament.round_progress(loaded_tournament.currently_round)
                match_organize.generate_peer()

                # Choose which player will play white
                match_organize.white_player()

                match_organize.select_match(tournament_choice)
                loaded_tournament.end_round(tournament_choice)
                match_organize.clear_checking(tournament_choice)
            if loaded_tournament.currently_round > loaded_tournament.round_number:
                end_tournament = True
        winner = match_organize.winner()
        view_tournament.end_tournament(winner)

    def recover_info(self):
        """Retrieves the tournament info needed to display it"""
        list_infos = []
        list_infos.append(self.name)
        list_infos.append(self.location)
        list_infos.append(self.start_date)
        list_infos.append(self.end_date)
        list_infos.append(self.round_number)
        list_infos.append(self.listed_player)
        return list_infos

    def random_mixed(self):
        """Randomly shuffle the list"""
        random.shuffle(self.listed_player)

    def established_first_round(self):
        """Returns a tuple list containing the organized matches of a round without scores"""
        list_all_match = []
        number_match = self.matchs_per_round()
        elem1 = 0
        elem2 = 1

        for i in range(number_match):
            tupl_match = (self.listed_player[elem1], self.listed_player[elem2])
            list_all_match.append(tupl_match)
            elem1 += 2
            elem2 += 2
        return list_all_match

    def matchs_per_round(self):
        """Returns the number of matches per round"""
        matchs = math.floor(len(self.listed_player) / 2)
        if len(self.listed_player) % 2 != 0:
            matchs = matchs + 1
        return matchs

    def end_round(self, id_tournament):
        """Updates the current round of the tournament"""
        data = DataTournament(id_tournament)
        data_match = DataMatch(id_tournament, self.currently_round)
        endtime = datetime.now().strftime("%Y-%m-%d_%Hh%M")

        data_match.end_time(endtime)

        # Increments tournament round by 1
        self.currently_round += 1

        # Retrieves tournament information
        data_update = self.get_data_tournament()

        # Save tournament data
        data.save_tournament(data_update)

    def peer_players(self, list_players):
        """Checks whether the number of players is even or not"""
        if len(list_players) % 2 != 0:
            return False
        else:
            return True
