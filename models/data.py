import os


class FolderCreate:
    def __init__(self):
        """Initialize folder paths"""
        self.path_data = "data/"
        self.path_tournament = "data/tournament/"
        self.path_report = "report/"
        self.path_checking = "checking/"

    def folder_report(self):
        """Create report/ folder if it doesn't exist"""
        self.path_exist(self.path_report)

    def folder_data(self):
        """Create data/ folder if it doesn't exists"""
        self.path_exist(self.path_data)

    def folder_tournament(self):
        """Create tournament/ folder if it doesn't exists"""
        self.path_exist(self.path_tournament)

    def folder_checking(self):
        """Create checking/ folder if it doesn't exists"""
        self.path_exist(self.path_checking)

    def path_exist(self, path):
        """Checks if the path exists, otherwise creates it"""
        if os.path.exists(path):
            pass
        else:
            os.mkdir(path)
